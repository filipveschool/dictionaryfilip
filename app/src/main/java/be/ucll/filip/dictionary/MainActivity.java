package be.ucll.filip.dictionary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private static final String LOG_TAG = "MainActivity";
    private List<Definition> mDefinitionsList = new ArrayList<Definition>();
    private RecyclerView mRecyclerView;
    private ArrayAdapter<Definition> arrayAdapter;

    //nieuw
    private ListView listApps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activateToolbar();

        listApps = (ListView) findViewById(R.id.xmlListView);

        arrayAdapter = new ArrayAdapter<Definition>(MainActivity.this, R.layout.list_item, new ArrayList<Definition>());
        listApps.setAdapter(arrayAdapter);





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        //dit wordt geactiveerd als je op de zoekknop drukt.
        if (id == R.id.menu_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //na het zoeken wordt de onResume functie uitgevoerd altijd.
        String query = getSavedPreferenceData(DEF_QUERY);
        if (query.length() > 0) {
            ProcessDefinitions processDefinitions = new ProcessDefinitions(query, true);
            processDefinitions.execute();
        }
    }


    private String getSavedPreferenceData(String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sharedPref.getString(key, "");
    }

    public class ProcessDefinitions extends GetXmlData {

        public ProcessDefinitions(String searchCriteria, boolean matchAll) {
            super(searchCriteria, matchAll);
        }

        public void execute() {
            //super.execute();
            ProcessData processData = new ProcessData();
            processData.execute();
        }

        public class ProcessData extends DownloadXmlData {

            protected void onPostExecute(String webData) {
                super.onPostExecute(webData);

                arrayAdapter = new ArrayAdapter<Definition>(MainActivity.this, R.layout.list_item, getDefinitions());
                listApps.setAdapter(arrayAdapter);

            }
        }
    }
}

