package be.ucll.filip.dictionary;

import android.net.Uri;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by filip on 6/11/2016.
 */

public class GetXmlData extends GetRawData {


    private String LOG_TAG = GetXmlData.class.getSimpleName();
    private List<Definition> mDefinitions;
    private Uri mDestinationUri;
    private String defUrl;
    private String xmlData;

    public GetXmlData(String searchCriteria, boolean matchAll) {
        super(null);
        createAndUpdateUri(searchCriteria, matchAll);
        mDefinitions = new ArrayList<Definition>();
    }

    public GetXmlData(String searchCriteria) {
        super(searchCriteria);
        //this.xmlData = xmlData;
        mDefinitions = new ArrayList<Definition>();
    }

    public List<Definition> getDefinitions() {
        return mDefinitions;
    }

    public void execute() {
        super.setmRawUrl(mDestinationUri.toString());
        DownloadXmlData downloadXmlData = new DownloadXmlData();
        Log.v(LOG_TAG, "Built URI = " + mDestinationUri.toString());
        downloadXmlData.execute(mDestinationUri.toString());
    }

    public boolean createAndUpdateUri(String searchCriteria, boolean matchAll) {
        final String XML_BASE_URL = "http://services.aonaware.com/DictService/DictService.asmx/Define?word=";

        defUrl = XML_BASE_URL + searchCriteria;

        Log.d("defUrl = ", defUrl);

        return defUrl != null;
    }

    public boolean process() {
        boolean status = true;
        Definition currentRecord = null;
        boolean inEntry = false;
        String textValue = "";

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            //xpp.setInput(new StringReader(this.xmlData));
            xpp.setInput(new StringReader(this.getmData()));
            int eventType = xpp.getEventType();


            while (eventType != XmlPullParser.END_DOCUMENT) {
                Log.d("eventtype:", "xpp eventtype + " + eventType);

                String tagName = xpp.getName();
                Log.d("tagname begin while:", "xpp getname + " + tagName);

//definitions is een start_tag en eventtype = 2
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        Log.d("ParseWords", "Starting tag for " + tagName);

                        if (tagName.equalsIgnoreCase("definitions")) {
                            Log.d("Definitions log", "inside definitions now.");
                            tagName = xpp.getName();
                        } else if (tagName.equalsIgnoreCase("definition")) {
                            inEntry = true;
                            currentRecord = new Definition();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        textValue = xpp.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        Log.d("ParseWords", "Ending tag for " + tagName);
                        if (inEntry) {
                            if (tagName.equalsIgnoreCase("definition")) {
                                mDefinitions.add(currentRecord);
                                inEntry = false;
                            } else if (tagName.equalsIgnoreCase("word")) {
                                currentRecord.setWord(textValue);
                            } else if (tagName.equalsIgnoreCase("worddefinition")) {
                                currentRecord.setWordDefinition(textValue);
                            } else if (tagName.equalsIgnoreCase("name")) {
                                currentRecord.setDictionary(textValue);
                            }
                        }
                        break;

                    default:
                        // Nothing else to do.

                } // end swtch eventtype
                eventType = xpp.next();

            }


        } catch (Exception e) {
            status = false;
            e.printStackTrace();
        }

        for (Definition def : mDefinitions) {
            Log.d("ParseDefinitions", "************************************");
            Log.d("ParseDefinitions", "Word: " + def.getWord());
            Log.d("ParseDefinitions", "WordDefinition: " + def.getWordDefinition());
        }

        return true;
    }


    public class DownloadXmlData extends DownloadRawData {

        protected void onPostExecute(String webData) {
            super.onPostExecute(webData);
            process();

        }

        protected String doInBackground(String... params) {
            String[] par = {defUrl.toString()};
            return super.doInBackground(par);
        }

    }

}