package be.ucll.filip.dictionary;

/**
 * Created by filip on 5/11/2016.
 */

public class Definition {

    private String word;
    private String wordDefinition;
    private String dictionary;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWordDefinition() {
        return wordDefinition;
    }

    public void setWordDefinition(String wordDefinition) {
        this.wordDefinition = wordDefinition;
    }

    public String getDictionary() {
        return dictionary;
    }

    public void setDictionary(String dictionary) {
        this.dictionary = dictionary;
    }


    @Override
    public String toString() {
        return "word: " + word + "\n" +
                "WordDefinition: " + wordDefinition + "\n" +
                "Dictionary: " + dictionary + "\n";
    }


}

