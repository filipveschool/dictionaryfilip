package be.ucll.filip.dictionary;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by filip on 5/11/2016.
 */

public class ParseWords {
    private String xmlData;
    private ArrayList<Definition> definitions;

    public ParseWords(String xmlData) {
        this.xmlData = xmlData;
        definitions = new ArrayList<Definition>();
    }

    public ArrayList<Definition> getDefinitions() {
        return definitions;
    }

    public boolean process() {
        boolean status = true;
        Definition currentRecord = null;
        boolean inEntry = false;
        String textValue = "";

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(this.xmlData));
            int eventType = xpp.getEventType();


            while (eventType != XmlPullParser.END_DOCUMENT) {
                Log.d("eventtype:", "xpp eventtype + " + eventType);

                String tagName = xpp.getName();
                Log.d("tagname begin while:", "xpp getname + " + tagName);

//definitions is een start_tag en eventtype = 2
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        Log.d("ParseWords", "Starting tag for " + tagName);

                        if (tagName.equalsIgnoreCase("definitions")) {
                            Log.d("Definitions log", "inside definitions now.");
                            tagName = xpp.getName();
                        }
                        //if (tagName.equalsIgnoreCase("entry")) {
                        else if (tagName.equalsIgnoreCase("definition")) {
                            inEntry = true;
                            currentRecord = new Definition();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        textValue = xpp.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        Log.d("ParseWords", "Ending tag for " + tagName);
                        if (inEntry) {
                            //if (tagName.equalsIgnoreCase("entry")) {
                            if (tagName.equalsIgnoreCase("definition")) {
                                definitions.add(currentRecord);
                                inEntry = false;
                            } else if (tagName.equalsIgnoreCase("word")) {
                                currentRecord.setWord(textValue);
                            } else if (tagName.equalsIgnoreCase("worddefinition")) {
                                currentRecord.setWordDefinition(textValue);
                            } else if (tagName.equalsIgnoreCase("name")) {
                                currentRecord.setDictionary(textValue);
                            }
                        }
                        break;

                    default:
                        // Nothing else to do.

                } // end swtch eventtype
                eventType = xpp.next();

            }


        } catch (Exception e) {
            status = false;
            e.printStackTrace();
        }

        for (Definition def : definitions) {
            Log.d("ParseDefinitions", "************************************");
            Log.d("ParseDefinitions", "Word: " + def.getWord());
            Log.d("ParseDefinitions", "WordDefinition: " + def.getWordDefinition());
            //Log.d("ParseDefinitions", "Release Date: " + def.getReleaseDate());
        }

        return true;
    }


}
